import interfaces.GebJiraDriver
import interfaces.JiraActor
import interfaces.JiraCredentials
import interfaces.JiraInstance
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver

class GebJiraDriverImpl implements GebJiraDriver {
    @Override
    def setConfig(String name) {
        return null
    }

    @Override
    def initialize() {
        return null
    }

    @Override
    JiraInstance getJiraInstance() {
        return null
    }

    @Override
    JiraCredentials getJiraCredentials() {
        return null
    }

    @Override
    JiraActor getJiraActor() {
        return null
    }

    @Override
    WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", getClass().getResource('/chromedriver.exe').getPath())
        WebDriver driver = new ChromeDriver()
        return driver
    }
}
