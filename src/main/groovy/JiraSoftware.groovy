import geb.Page
import interfaces.JiraInstance
import pages.Login

class JiraSoftware implements JiraInstance {
    private final String url

    JiraSoftware(String hostUrl){
        url = hostUrl.reverse().startsWith("/") ? hostUrl.replaceFirst("\\/\$", "") : hostUrl
    }

    @Override
    String getUrl() {
        return url
    }

}
