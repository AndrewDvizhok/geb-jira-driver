import interfaces.JiraCredentials

class JiraUser implements JiraCredentials{
    private final String username
    private final String password
    private final boolean is2fa
    private final boolean isSso

    JiraUser(Properties properties){
        username = properties."username"
        password = properties."password"
        is2fa = Boolean.getBoolean(properties."is2fa")
        isSso = Boolean.getBoolean(properties."isSso")
    }

    @Override
    String getUsername() {
        return username
    }

    @Override
    String getPassword() {
        return password
    }

    @Override
    boolean is2fa() {
        return is2fa
    }

    @Override
    boolean isSso() {
        return isSso
    }
}
