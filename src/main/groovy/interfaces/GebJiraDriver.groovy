package interfaces

import org.openqa.selenium.WebDriver

interface GebJiraDriver {
    setConfig(String name)
    initialize()
    JiraInstance getJiraInstance()
    JiraCredentials getJiraCredentials()
    JiraActor getJiraActor()
    WebDriver getDriver()
}