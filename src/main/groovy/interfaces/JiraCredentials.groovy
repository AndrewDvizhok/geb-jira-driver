package interfaces

interface JiraCredentials {
    String getUsername()
    String getPassword()
    boolean is2fa()
    boolean isSso()
}