package interfaces

import geb.Page

interface JiraInstance {
    String getUrl()
}