package interfaces

import geb.Page
import org.openqa.selenium.WebDriver

interface JiraActor {
    final int JIRA_TIMEOUT = 10
    final int JIRA_SHORT_TIMEOUT = 3

    boolean login(JiraCredentials jiraCredentials)

    boolean logout()

    boolean sudoConfirm(JiraCredentials jiraCredentials)

    boolean isPage(Page page)

    boolean isLoggedIn()

    String getProfileName()

    boolean unScreenFieldConfiguration(int fieldConfigurationId, JiraCredentials jiraCredentials)

    setWebDriver(WebDriver driver)

    exit()
}