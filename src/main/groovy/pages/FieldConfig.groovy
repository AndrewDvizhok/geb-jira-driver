package pages

class FieldConfig {
    static url = "/secure/admin/ConfigureFieldLayout!default.jspa"

    static content = {
        name { $("b", id: "field-layout-name") }
        table { $("table", id: "field_table") }
    }
}
