import interfaces.GebJiraDriver
import interfaces.JiraActor
import interfaces.JiraCredentials
import interfaces.JiraInstance
import org.openqa.selenium.WebDriver

println "Script run by groovy version ${GroovySystem.version}"
println "Read default properties"

Properties properties = new Properties()
try {
    File configFile = new File(getClass().getResource('/default.conf').toURI())
    configFile.withInputStream {
        properties.load(it)
    }
    println "Found host name: "+properties."host"
}
catch (FileNotFoundException exp) {
    System.err.println("Config not found.  Reason: " + exp.getMessage())
}

println "Create Jira instance"

JiraInstance jiraInstance = new JiraSoftware(properties."host")

println "Create credentials"

JiraCredentials jiraCredentials = new JiraUser(properties)

println "Create actor"

JiraActor actor = new Worker(jiraInstance)

GebJiraDriver gjd = new GebJiraDriverImpl()
WebDriver driver = gjd.getDriver()
actor.setWebDriver(driver)

if(properties."cleanScreenFieldConfig" != null){
    println "Try clean screen on field config!"
    actor.unScreenFieldConfiguration(Integer.valueOf(properties."cleanScreenFieldConfig"), jiraCredentials)
}

actor.exit()