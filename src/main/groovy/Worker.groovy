import geb.Browser
import geb.Page
import geb.waiting.WaitTimeoutException
import interfaces.JiraActor
import interfaces.JiraCredentials
import interfaces.JiraInstance
import org.openqa.selenium.WebDriver
import pages.*


class Worker implements JiraActor{
    private final Browser browser
    private final JiraInstance jiraInstance

    Worker(JiraInstance instance){
        browser = new Browser()
        jiraInstance = instance
        browser.baseUrl = instance.getUrl()
    }

    @Override
    boolean login(JiraCredentials jiraCredentials) {
        String savedCurrentUrl = browser.currentUrl
        browser.to Login
        if(jiraCredentials.is2fa() || jiraCredentials.isSso()){
            println "Wait manual login: 2fa: "+jiraCredentials.is2fa() +", sso: "+jiraCredentials.isSso()
            int tooLong = JIRA_TIMEOUT
            while( !isLoggedIn() && (tooLong)>0 ){
                tooLong -= JIRA_SHORT_TIMEOUT
            }
            if(tooLong>=0){
                System.err.println("Too long login")
            }
        }else{
            try{
                browser.waitFor(JIRA_TIMEOUT) { browser.$("input", id: "login-form-username").isDisplayed() }
            }catch(WaitTimeoutException exception){
                System.err.println("Can't login: "+exception.getMessage())
                return false
            }
            if (browser.form.isDisplayed()) {
                browser.formLogin = jiraCredentials.getUsername()
                browser.formPass = jiraCredentials.getPassword()
                browser.formSend.click()
            }
        }
        if(isLoggedIn()){
            browser.go savedCurrentUrl
            return true
        }
        return false
    }

    @Override
    boolean logout() {
        return false
    }

    @Override
    boolean sudoConfirm(JiraCredentials jiraCredentials) {
        if(!isLoggedIn()){
            login(jiraCredentials)
        }
        try{
            browser.waitFor(JIRA_SHORT_TIMEOUT) { browser.$("input", id: "login-form-authenticatePassword").isDisplayed() }
            browser.$("input", id: "login-form-authenticatePassword").value(jiraCredentials.getPassword())
            browser.$("input", id:"login-form-submit").click()
        }catch(WaitTimeoutException exception){
            println("No need confirm web sudo")
            return false
        }
        return true
    }

    @Override
    boolean isPage(Page page) {
        return false
    }

    @Override
    boolean isLoggedIn() {
        try{
            browser.waitFor(JIRA_SHORT_TIMEOUT) { browser.$("a", id: "header-details-user-fullname").isDisplayed() }
            return true
        }catch(WaitTimeoutException exception){
            System.err.println("Don't logged yet: "+exception.getMessage())
            return false
        }
        return false
    }

    @Override
    boolean unScreenFieldConfiguration(int fieldConfigurationId, JiraCredentials credentials) {
        browser.go FieldConfig.url + "?id=" + fieldConfigurationId
        sudoConfirm(credentials)
        try {
            browser.waitFor(JiraActor.JIRA_SHORT_TIMEOUT) { browser.$("table", id: "field_table").isDisplayed() }
            Set<String> linksToScreen = new HashSet<>()
            browser.$("#field_table tr").each {row->

                if(row.$('td')[1].text() != null){
                    String link = row.find('*[id*=associate]').attr('href')
                    if(link != null){
                        linksToScreen.add(row.find('*[id*=associate]').attr('href'))
                    }
                }
            }
            if(linksToScreen.size() > 0){
                linksToScreen.each {screen ->
                    resetScreens(screen, credentials)
                }
            }
        } catch (WaitTimeoutException exception) {
            System.err.println("Can't open Field config: " + exception.getMessage())
            return false
        }



        return true
    }

    private boolean resetScreens(String link, JiraCredentials credentials){
        browser.go link
        sudoConfirm(credentials)
        try {
            browser.waitFor(JiraActor.JIRA_TIMEOUT) { browser.$("input", id: "update_submit").isDisplayed() }
            println "Try reset screens"
            browser.driver.executeScript("\$('input[name=associatedScreens]').each(function(i){\$(this).prop( \"checked\", false );});")
            browser.$("input", id: "update_submit").click()
            return true
        } catch (WaitTimeoutException exception) {
            System.err.println("Screen didn't load: " + exception.getMessage())
            return false
        }
        return false
    }

    @Override
    def setWebDriver(WebDriver driver) {
        browser.setDriver(driver)
    }

    @Override
    String getProfileName() {
        browser.to Profile
        browser.waitFor(JIRA_TIMEOUT) { browser.$("span", id: "up-user-title-name").isDisplayed() }
        return browser.username.isDisplayed() ? browser.username : null
    }

    WebDriver getDriver(){
        return browser.getDriver()
    }

    @Override
    exit(){
        browser.getDriver().quit()
        Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T")
    }
}